FROM httpd
LABEL seongwon seongwon@mz.co.kr 

COPY . /usr/local/apache2/htdocs/
COPY ./httpd.conf /usr/local/apache2/conf/httpd.conf

EXPOSE 80 
